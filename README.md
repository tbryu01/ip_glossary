IpGrossarySystem
---
# Description
IPで使用されている用語の一覧表示、登録、削除を行うWebアプリケーションです。

# 事前準備

## データベースの作成
grossary_dbの作成
```bash
create database if not exists glossary_db;
```

## テーブルの準備
glossary_tblの作成
```bash
create table if not exists glossary_tbl (
	id int not null auto_increment,
	name varchar(30) not null,
	mena text,
	update_time timestamp(6) not null default current_timestamp(6) on update current_timestamp(6),
  primary key (id)
) engine = INNODB;
```

## 初期化
glossary_tblにデータを追加
```bash
insert into glossary_tbl (name, mean) values
('EasyID', '楽天会員の登録をした際に付与されるユニークな番号'),
('オーソリ', 'クレジットカードの有効性確認'),
('契約応答日', 'ご契約後に迎える毎月または毎年の契約日に応答する日のこと'),
('失効', '猶予期間を過ぎても保険料の払込がない場合に、契約が効力を失うこと'),
('謝絶', '保険契約引受不可');
```

## application.propertiesの設定
application.propertiesに自分のユーザー名とパスワードを記載
```
spring.jpa.hibernate.ddl-auto=none
spring.datasource.url=jdbc:mysql://localhost:3306/glossary_db
spring.datasource.username=/*ユーザー名記載*/
spring.datasource.password=/*パスワード記載*/
```

# アプリケーションの実行
```
http://localhost:8080/glossary
```

package com.example.grossary;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class IpGrossarySystemApplication {

	public static void main(String[] args) {
		SpringApplication.run(IpGrossarySystemApplication.class, args);
	}

}

package com.example.grossary;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class GlossaryController {
	
	@Autowired
	private GlossaryService glossaryService;
	@Autowired
	private GlossaryRepository glossaryRepository;

	/**
	 * 
	 * @param model
	 * @return
	 */
	@RequestMapping(path = "/glossary", method = RequestMethod.GET)
	public String index(Model model) {
		List<GlossaryEntity> list = glossaryRepository.findAll();
		model.addAttribute("list", list);
		return "index";
	}

	/**
	 * 新規登録画面を表示
	 * @param model
	 * @return
	 */
	@RequestMapping(path = "/insert", method = RequestMethod.GET)
	public String insert(Model model) {
		return "insert";
	}
	
	/**
	 * 
	 * @param glossaryForm
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/create", method = RequestMethod.POST)
	public String create(@ModelAttribute GlossaryForm glossaryForm, Model model) {
		glossaryService.create(glossaryForm);
		
		return "redirect:/glossary";
	}
	
	@RequestMapping(value = "/delete/{id}", method = RequestMethod.GET)
	public String delete(@PathVariable Integer id, Model model) {
		glossaryService.delete(id);
		
		return "redirect:/glossary";
	}
}

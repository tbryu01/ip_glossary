package com.example.grossary;

import javax.validation.constraints.NotNull;

public class GlossaryForm{
	
	@NotNull
	private String name;
	private String mean;
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public String getMean() {
		return mean;
	}
	
	public void setMean(String mean) {
		this.mean = mean;
	}

}

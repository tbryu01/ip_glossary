package com.example.grossary;

import java.sql.Timestamp;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PathVariable;

@Service
public class GlossaryService {
	
	@Autowired
	GlossaryRepository glossaryReposityory;
	
	/**
	 * 用語集全検索
	 */
	
	
	/**
	 * 用語新規登録
	 * @param glossaryForm
	 */
	public void create(GlossaryForm glossaryForm) {
		glossaryReposityory.save(createGlossary(glossaryForm));
	}
	
	/**
	 * 
	 * @param id
	 */
	public void delete(@PathVariable Integer id) {
		glossaryReposityory.deleteById(id);
	}
	
	/**
	 * 
	 * @param glossaryForm
	 * @return
	 */
	private GlossaryEntity createGlossary(GlossaryForm glossaryForm) {
		
		GlossaryEntity glossary = new  GlossaryEntity();
		
		glossary.setName(glossaryForm.getName());
		glossary.setMean(glossaryForm.getMean());
		glossary.setTimeStamp(new Timestamp(System.currentTimeMillis()));
		
		return glossary;
	}
}
